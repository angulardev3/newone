import { AfterContentChecked, AfterContentInit, AfterViewChecked, Component, DoCheck, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnChanges,OnInit,DoCheck,AfterContentInit,AfterViewChecked{
  title = 'newone';
  testing:string=""

  ngOnChanges(){
    // console.log("i am ngOnChanges")
  }

  ngOnInit(){
    // console.log("i am ngOnInit");
  }

  ngDoCheck(){
    // console.log("i am ngDoCheck")
  }
  ngAfterContentInit(){
    // console.log("i am ngAfterContentInit");
  }

  ngAfterContentCheck(){
    // console.log("i am ngAfterContentInit");
  }
  ngAfterViewInit(){
    // console.log("i am ngAfterViewInit");
  }

  ngAfterViewChecked(){
    // console.log("i am ngAfterViewChecked");
  }
  ngOnDestroy(){
    // console.log("i am ngOnDestroy");
  }
}
