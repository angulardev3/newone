import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { UserComponent } from './user/user.component';
import { PaginationComponent } from './pagination/pagination.component';
import { RouterModule, Routes } from '@angular/router';
import { CustomerrorComponent } from './customerror/customerror.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { HttpClientModule } from '@angular/common/http';
import { PipeComponent } from './pipe/pipe.component';
import { TemplateDrivenComponent } from './template-driven/template-driven.component';
import { ReactiveComponent } from './reactive/reactive.component';

const routes:Routes=[
  {path:"",redirectTo:"/home",pathMatch:"full"},
{path:"home",component:HomeComponent},
{path:"child",component:ChildComponent},
{path:"parent",component:ParentComponent},
{path:"pagination",component:PaginationComponent},
{path:"user",component:UserComponent},
{path:"rxjs",component:RxjsComponent},
{path:"reactive", component:ReactiveComponent},
{path:"**",component:CustomerrorComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ParentComponent,
    ChildComponent,
    UserComponent,
    PaginationComponent,
    RxjsComponent,
    PipeComponent,
    TemplateDrivenComponent,
    ReactiveComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
