import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent {
  parentTitle:any="parent data";
  parentFn(data:any){
    console.log(data);
  }
  constructor(private router:Router){
    
  }
  onClick(){
    //there are two types navigateByUrl and navigate
    // this.router.navigateByUrl('/child')
    this.router.navigate(['/child'])
  }
}
