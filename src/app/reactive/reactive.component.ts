import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {
  customerForm:any=FormGroup;

  ngOnInit(){
    this.customerForm=new FormGroup({
        firstName:new FormControl('',Validators.required),
        lastName:new FormControl('',Validators.required),
        email:new FormControl('',Validators.required),
        companyName:new FormControl('',Validators.required),
        phone:new FormControl('',Validators.required),
        address:new FormControl('',Validators.required),
    })
  }

  onSubmit(){
    console.log("customer form",this.customerForm.value.firstName);
  }
}
