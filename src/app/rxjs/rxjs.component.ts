import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable,Observer,of,Subscriber, Subscription, count, interval, map, mapTo, filter, fromEvent, switchMap, debounceTime, mergeMap, concatMap, tap, BehaviorSubject, Subject } from 'rxjs';
import { ApiService } from '../services/api.service';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.css']
})
export class RxjsComponent implements OnInit,OnDestroy {

constructor(private httpService:ApiService ){}
intervaSubscription:any=Subscription;
counter=0;
searchControl:FormControl=new FormControl();
ngOnInit(){
  
  //subject and behaviour subject
    let behaviourSubject$=new Subject();

    behaviourSubject$.subscribe((data)=>{
      console.log("A",data)
    })
    behaviourSubject$.next(1);
    
    behaviourSubject$.subscribe((data)=>{
      console.log("B",data)
    })
    behaviourSubject$.next(2);
 
    
  //TakeUtnil operator


  //tap operator it cannot change orignal value
    // of(1,2,3,4,5).pipe(
    //   tap((val)=>{
    //     console.log(val+2)
    //   })
    // ).subscribe()

  //debounceTime operator
    // this.searchControl.valueChanges.
    // pipe(
    //   debounceTime(1000)
    // ).subscribe(console.log)
  //mergeMap operator
    // It will call parallel
    // of(1,2,3,4)
    // .pipe(mergeMap((num)=>of(`${num}`))).subscribe(console.log)

  //concatMap operator
  //it will execute one by one
  // of(3,4,5,6)
  // .pipe(concatMap((num)=>of(`${num}`))).subscribe(console.log)

  //switchMap operator
    // fromEvent(document,'click').pipe(
    //   switchMap(()=>interval(500))
    // ).subscribe((val)=>this.counter=val).unsubscribe()
    // this.searchControl.valueChanges.pipe(
    //   debounceTime(1000),
    //   switchMap(()=>interval(1000))
    // ).subscribe((val:any)=>this.counter=val)


  //map operator
    // this.httpService.getUrl('https://jsonplaceholder.typicode.com/todos').pipe(
    //   map((res:any)=>res.map((data:any)=>{
    //     return {
    //       id:data,
    //       completed:data.completed
    //     }
    //   }))
    // ).subscribe(console.log)

//*custom observable
// const customOservable=Observable.create((observer: Observer<any>)=>{
//   let count=0;
//   setTimeout(()=>{
//     observer.next(count)
//     count++;
//   },100)
// })
// customOservable.subscribe((data:any)=>{
//   console.log(data)
// })

//   const foo=new Observable((subscriber)=>{
//     console.log("hello");
//     subscriber.next(42);
//   }) 
//  foo.subscribe((x)=>{
// console.log(x);
//  })

    // this.intervaSubscription= interval(1000).subscribe(count=>{
    //   console.log(count);
    // })
  }

  ngOnDestroy(): void {
    // this.intervaSubscription.unsubscribe();
  }
}
