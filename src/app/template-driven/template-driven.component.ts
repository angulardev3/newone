import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.css']
})
export class TemplateDrivenComponent {
firstName:string="";
lastName:string="";

addCustomer(formValue:NgForm){
  console.log(formValue)
}
}
