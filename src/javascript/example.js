
// const arr=[1,2,3,4]
// const obj={x:1,y:2}
// console.log([...arr],{...obj});


// const arr=[[10],[20],,389,39489,3[[[[[46]]]]],[30],[40]]
// // const res=arr.reduce((acc,curr)=>{
// //     return acc.concat(curr);
// // })
// // console.log(res)
// console.log(arr.flat(5))
// const obj1={
//     name:"manoj",
//     age:30
// }

// const obj2=obj1;
// obj2.age=25;
// console.log(obj1);
// console.log(obj2);

// const str1=["Hithere","Universe"];
// const str2=["Hithere","Universe"]

// console.log(str1==str2);

// var name="manoj"
// console.log(`${name}`)

// console.log(5-"2")

// let arr=[1];
// arr.length=4;
// console.log(arr);
// arr[3]=2;
// console.log(arr)
// for(let i in arr){
//     console.log(i);
// }
// const profile={
//     name:"Manoj",
//     design:"UI Engineer"
// }
// //allow to update any propery but freeze will not
// Object.seal(profile);
// profile.name="Deepa";
// profile.age=27;
// delete profile.name
// console.log(profile.name);
// const base={
//     a:1
// }
// const obj=Object.freeze(base);
// console.log(obj)
// base.y=2;
// console.log(base.y);
// console.log("hi" instanceof Object)

// console.log(5+'5');
// console.log(5-"5");

// console.log(NaN===NaN);
// console.log(NaN==NaN);

// (function(){
//     var a=b=5;
// })();
// console.log(a);
// console.log(typeof a)

// function* gen1(){
//     yield[1,2,3,4];
// }
// function* gen2(){
//     yield*[1,2,3,4]
// }
// const t1=gen1();
// const t2=gen2();
// console.log(t1.next().value);
// console.log(t2.next().value);

// var obj={
//     x:3,
//     fun:function(){
//         console.log(this.x);
//     }
// }
// obj.fun();
// new obj.fun();

// function calcu(num){
//     return num*0.01;
// }
// console.log(typeof calcu('hello'))

// var x=0;
// if(10>20>30){
//     x+=1;
// }
// console.log(x)
// let x={
//     a:1,
//     b:2
// }
// let y={
//     b:3,
//     c:4
// }
// // Object.setPrototypeOf(x,y);
// console.log(x.b)
// const obj={
//     timeoutId:setTimeout(()=>{
//         console.log('hi');
//     },1000)
// }
// delete obj.timeoutId;
// obj=null;
// console.log(obj.timeoutId)

// var bar=function foo(){};
// console.log(bar===foo);

// let foo=function(){
//     console.log(1);
// }
// foo=function(){
//     console.log(2)
// }
// setTimeout(foo,1000);


// let elf='Estel';
// function lapland(){
//     console.log(elf);
//     let elf="arven";
// }
// lapland();

// const obj={x:1,y:2}
// let{x:a,y:b}=obj
// a=2;
// console.log(x)

// let x=100;
// let y=x++;
// let z=++x;
// let n=(x==y)?z++:++z;
// console.log(n)

// var f=new Boolean(false);
// if(f){
//     console.log(1);
// }else{
//     console.log(2);
// }
// console.log(typeof f)