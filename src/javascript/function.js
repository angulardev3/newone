const sumr=[1,2,3,4,4];
console.log(...sumr)

// const nestedarr=[1,[2],5,3,2,4,5,[3],[4]];
// const arr2=nestedarr.flat(2);
// console.log(...new Set(arr2));

// const arr1=[1,2,3,4];
// const arr2={5:10};
// console.log(Array.isArray(arr2));

// const arr=[1,2,3,4];
// const sum=arr.map((num)=>console.log(num))

// class Bird {
//     constructor(name) {
//       this.name = name;
//     }
//     speak() {
//       console.log(`${this.name} makes a noise.`);
//     }
//   }
//    class Crow extends Bird{
//     speak() {
//       super.speak();
      
//       console.log(`${this.name} sings.`);
//     }
//   }
//    const crow = new Crow("Tim");
//   crow.speak();
// const arr=["India","USA"];
// const [first,second]=arr;
// console.log(first,second)

// const obj1={
//     prop1:"value1",
//     prop2:"raj"
// }
// const {key1,key2}=obj1;
// console.log(key1)
// const obj1={
//     prop1:"value1",
//     prop2:{
//         prop3:"value2"
//     }
// }
// const {prop1,prop2}=obj1;
// console.log(prop1)
// newObj.prop2.prop3="newvalue3";
// console.log(obj1.prop2.prop3)

// const person = {
//     name: "John",
//     logName: () => {
//       console.log(this.name); // logs "John"
//     }
//   };
//   person.logName(); // logs "John"

// const promise1=Promise.resolve("one");
// console.log(promise1)
// const promise2=new Promise((resolve)=>setTimeout(()=>
//     resolve("Two"),1000));
// const promise3=Promise.reject("three");
// Promise.allSettled([promise1,promise2,promise3]).then((result)=>console.log(result));

// setTimeout(function(){console.log("hello anonyms function")},1000)
//recursive function
// function countDown(num){
//     console.log(num);
//     num--
//     setTimeout(()=>{
       
//     if(num>=0){
       
//         countDown(num);
//     }
//     },1000)
// }
// countDown(10);
// function myFn(){
//     if(condition){
//         myFn()
//     }else{
//         myFn();
//     }
 
// }
// myFn();
// let sum=function(num){
//     return num*2
// }
// console.log(sum(2))

// const obj={
//     x:20,
//     rc1:function(){
//         console.log(this.x)
//     },
//     rc2:()=>{
//         console.log(this.x)
//     }
// }
// obj.rc1();
// obj.rc2();
// //callback function
// function greet(name){
//     console.log("hello "+name);
// }
// function process(callback){
//     const name="manoj"
//     callback(name);
// }
// process(greet);

//rest vs spread opearators
// const fn=(a,x,y,...numbers)=>{
//     console.log(x,y,...numbers);
// }
// fn(1,2,3,4,5,6,5,4)


// function square(...nums){ //rest operator
//     console.log(num1+num2)
// }
// var arr=[1,2];
// square(...arr);//spread

//IIFE immediatly invoked function
// const calc=(function square(num){
//     return num*num;
// })(5)
// console.log(calc)
// ex:2
// (function (x){
//     return (function(y){
//         console.log(x);
//         console.log(y)
//     })(2);
// })(5);


// //first class function A function can be used in another function and manupulate that function
// function square(num){
//     return num*num;
// }
// function displayFn(fn){
//     console.log("this is "+fn(5))
// }
// displayFn(square)


// // function expression or anynymos when we store inside variable
// const myFn=function(num){
//     return num*num;
// }
// console.log(myFn(10))

// //function declaration
// function sum(num){
//     return num+num;
// }
// console.log(sum(2))

